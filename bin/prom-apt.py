#! /usr/bin/env python

"""
    Update aptitude cache and check for package updates.  Will display if it is in the security repo or not.
    
    Script is intended to run in python 2.7+ with no dependencies.
"""

import argparse
import grp
import os
import pwd
import shutil
import subprocess
import tempfile


def is_apt_available():
    if not os.path.exists("/usr/bin/aptitude"):
        raise RuntimeError("/usr/bin/aptitude does not exist")


def refresh_apt_cache():
    cmd = ["/usr/bin/aptitude", "update"]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if int(p.returncode) != 0:
        raise RuntimeError("aptitude cache update failed")        

        
def get_available_updates():

    update_count = 0
    security_update_count = 0
    available_updates = []

    cmd = ["/usr/bin/aptitude", "search", "-F", "%p %t", "--disable-columns", "~U"]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if int(p.returncode) != 0:
        raise RuntimeError("aptitude search failed")
        
    for line in out.split(os.linesep):
        try:
            sline = line.split()
            
            this_package = {
                "name": sline[0],
                "repos": sline[1].split(","),
            }
            
            if sline[1].lower().find("security") != -1:
                this_package["security"] = True
                security_update_count = security_update_count + 1
            else:
                this_package["security"] = False
            
            update_count = update_count + 1
            available_updates.append(this_package)
        except IndexError:
            # probably a blank line
            pass
    return update_count, security_update_count, available_updates


def reboot_required():
    if os.path.exists("/var/run/reboot-required"):
        return 1
    else:
        return 0


def set_permissions(file, owner, group, mode):
    os.chmod(file, mode)
    os.chown(
        file,
        pwd.getpwnam(owner).pw_uid,
        grp.getgrnam(group).gr_gid,
    )


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--textfile", type=str, default=None, help="Location of the text file created for Prometheus scraping.")
    parser.add_argument("-o", "--owner", type=str, default="root", help="Owner of the text file created.")
    parser.add_argument("-g", "--group", type=str, default="root", help="Group of the text file created.")
    parser.add_argument("-m", "--mode", type=int, default=644, help="File permission mode of the text file created.")
    parser.add_argument("-q", "--quiet", action='store_true', default=False, help="Suppress Output")
    args = parser.parse_args()

    # safety check
    is_apt_available()
    
    # refresh packages
    refresh_apt_cache()
    
    # pull what needs to be updated
    update_count, security_update_count, available_updates = get_available_updates()
    
    # store script output
    script_output = [
        "apt_upgrades_pending %s" % update_count,
        "apt_security_upgrades_pending %s" % security_update_count,
        "node_reboot_required %s" % reboot_required(),
    ]

    # print to stdout
    if not args.quiet:
        for i in script_output:
            print(i)


    if args.textfile:

        if not os.path.isdir(os.path.split(args.textfile)[0]):
            raise OSError("The parent directory for %s does not exist." % args.textfile)

        # create temp dir and a temp file with the text file name inside.
        tempdir = tempfile.mkdtemp()
        tempout = os.path.join(
            tempdir,
            os.path.split(args.textfile)[1],
        )

        # open the temp file and write to it.
        with open(tempout, "w") as tempoutd:
            for line in script_output:
                tempoutd.write(line)
                tempoutd.write(os.linesep)

        # set permissons as defined by the script.
        set_permissions(tempout, args.owner, args.group, args.mode)

        # move the out file and remove temp dir.
        shutil.move(tempout, args.textfile)
        os.rmdir(tempdir)

        # set the final file permissions
        set_permissions(args.textfile, args.owner, args.group, args.mode)
